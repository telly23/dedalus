'use strict';

var smallCabin = {
    name: "cabin",
    fullName: "A small cabin",
    description: {
	def: "There's dust everywhere"
    },
    accessLocations: []
};

var forestClairing = {
    name: "clairing",
    fullName: "A small clairing",
    description: {
	def: "Old branches crack under your feet"
    },
    accessLocations: []
};

var hiddenPath = {
    name: "hidden path",
    fullName: "A hidden path",
    description: {
	def: "The path is hard to follow"
    },
    accessLocations: []
};

var forest = {
    name: "forest",
    fullName: "A dark forest",
    description: {
	def: "A dark and silent forest"
    },
    startLocation: [forestClairing, smallCabin]
};

smallCabin.accessLocations = [[forestClairing, "Go outside", 1]];

forestClairing.accessLocations = [[smallCabin, "Enter the cabin", 1],
				  [hiddenPath, "Follow the hidden path", 0.25]];

hiddenPath.accessLocations = [[forestClairing, "Follow the path to the clairing", 1]]

console.log("Forest loaded");
